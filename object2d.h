#pragma once
#include "math.h"

class Game;

class Object2D {
	public:
		Object2D();
		virtual ~Object2D() {};

		virtual void Update(float delta) = 0;
		const Vector2 GetPosition() const { return object_position; }
		void SetPosition(const Vector2 pos) { object_position = pos; }

	private:
		Vector2 object_position;
};