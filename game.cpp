#include "game.h"

const int MS_PER_UPDATE = 8;
constexpr float delta = MS_PER_UPDATE / 1000.0f;

Game::Game() :
	window_ptr(nullptr),
	renderer_ptr(nullptr),
	music_ptr(nullptr),
	is_running(true),
	lag_time(0),
	previous_time(0),
	tick_counter(0),
	paddle_height(64),
	thickness(8)
{}

Game::~Game() {
	if (renderer_ptr != nullptr) {
		SDL_DestroyRenderer(renderer_ptr);
	}
	if (window_ptr != nullptr) {
		SDL_DestroyWindow(window_ptr);
	}
	renderer_ptr = nullptr;
	window_ptr = nullptr;

	Mix_FreeMusic(music_ptr);

	UnloadData();

	Mix_Quit();
	SDL_Quit();
}

bool Game::Initialize() {
	// Initialize SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0) {
		SDL_Log("Unable to initialize SDL: %s", SDL_GetError());
		return false;
	}

	// Initialize SDL_TTF
	if (TTF_Init() < 0) {
		SDL_Log("Unable to initialize TTF: %s", TTF_GetError());
		return false;
	}

	// Initialize SDL_Mixer
	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048 ) < 0) {
		printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
		return false;
	}

	window_ptr = SDL_CreateWindow(
		"Game", // Window name
		100,    // Top left x coord offset
		100,	// Top left y coorf offset
		640,	// Width of window
		480,	// Height of window
		0);		// Flags (0 for none)

	if (!window_ptr) {
		SDL_Log("Failed to create window: %s", SDL_GetError());
		return false;
	}

	renderer_ptr = SDL_CreateRenderer(
		window_ptr,	// Window to create a renderer
		-1,			// Usually -1
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC); // Flags

	if (!renderer_ptr) {
		SDL_Log("Failed to create renderer: %s", SDL_GetError());
		return false;
	}
	LoadData();

	return true;
}

void Game::LoadData() {
	// Create Game Object
	score_ptr = new Score();
	paddle_ptr = new Paddle(8.0f, 240.0f);
	paddleAI_ptr = new Paddle(640.0f - 16.0f, 240.0f);
	ball_ptr = new Ball(320.0f, 240.0f);

	music_ptr = Mix_LoadMUS("Russian.mp3");
	if (music_ptr == nullptr) {
		SDL_Log("Failed to load medium sound effect! SDL_mixer Error: %s\n", Mix_GetError());
	}

	Mix_PlayMusic(music_ptr, -1);
}

void Game::UnloadData() {
	// Delete data
	delete paddleAI_ptr;
	delete paddle_ptr;
	delete ball_ptr;
	delete score_ptr;
}

void Game::ProcessInput() {
	SDL_Event event;

	// While there are events in queue
	while (SDL_PollEvent(&event)) {
		switch (event.type) {
		case SDL_QUIT:
			is_running = false;
			break;
		}
	}

	// Get keyboard state
	const Uint8* state = SDL_GetKeyboardState(NULL);
	// If escape is pressed, leave loop too
	if (state[SDL_SCANCODE_ESCAPE]) {
		is_running = false;
	}
	if (state[SDL_SCANCODE_P]) {
		// Pause Game
	}

	paddle_ptr->ProcessKeyboard(state);
}

void Game::UpdateGame() {
	Uint32 current_time = SDL_GetTicks();
	
	lag_time += current_time - previous_time;

	while (lag_time >= MS_PER_UPDATE) {
		tick_counter += MS_PER_UPDATE;

		ball_ptr->Collision(paddle_ptr, paddleAI_ptr);
		paddleAI_ptr->AIDirection(ball_ptr->GetPosition());

		ball_ptr->Update(delta);
		paddle_ptr->Update(delta);
		paddleAI_ptr->Update(delta);

		lag_time -= MS_PER_UPDATE;
	}

	ball_ptr->Collision(paddle_ptr, paddleAI_ptr);

	ball_ptr->Update(lag_time / 1000.0f);
	paddle_ptr->Update(lag_time / 1000.0f);
	paddleAI_ptr->Update(lag_time / 1000.0f);

	lag_time = 0;

	previous_time = current_time;

	// This should be inside a class
	// -------------------------------------------
	// Check if a gold was scored
	Vector2 ball = ball_ptr->GetPosition();
	if (ball.x < 0) {
		AIScored();
	}
	else if (ball.x > 640) {
		PlayerScored();
	}
	// -------------------------------------------
}

void Game::GenerateOutput() {
	BackgroundColor(renderer_ptr);

	// Clear back buffer and draw the front buffer
	SDL_RenderClear(renderer_ptr);
	
	ForegroundColor(renderer_ptr);

	paddle_ptr->DrawObject2d(renderer_ptr);
	paddleAI_ptr->DrawObject2d(renderer_ptr);
	ball_ptr->DrawObject2d(renderer_ptr);
	score_ptr->DrawScore(renderer_ptr);

	// Swap the front and back buffers
	SDL_RenderPresent(renderer_ptr);
}

void Game::BackgroundColor(SDL_Renderer* renderer_ptr) {
	SDL_SetRenderDrawColor(
		renderer_ptr,
		0,		// Red
		0,		// Green
		0,		// Blue
		255);	// Alpha
}

void Game::ForegroundColor(SDL_Renderer* renderer_ptr) {
	SDL_SetRenderDrawColor(
		renderer_ptr,
		128 * cos(tick_counter / 1000.0f) + 127,
		128 * cos(tick_counter / 1000.0f + 2 * (Math::Pi / 3)) + 127,
		128 * cos(tick_counter / 1000.0f + 4 * (Math::Pi / 3)) + 127,
		255);
}

int Game::GetPaddleHeight() {
	return paddle_height;
}

int Game::GetThickness() {
	return thickness;
}

// I should probably make an event system for this
void Game::PlayerScored() {
	score_ptr->PlayerScored();
	delete ball_ptr;
	ball_ptr = new Ball(320.0f, 240.0f);
}

void Game::AIScored() {
	score_ptr->AIScored();
	delete ball_ptr;
	ball_ptr = new Ball(320.0f, 240.0f);
}

bool Game::KeepRunning() {
	return is_running;
}