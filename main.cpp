#include "game.h"

int main(int argc, char** agrv) {
	Game* game_ptr = new Game();

	if (game_ptr->Initialize()) {
		while (game_ptr->KeepRunning()) {
			game_ptr->ProcessInput();
			game_ptr->UpdateGame();
			game_ptr->GenerateOutput();
		}
	}

	game_ptr->~Game();

	return 0;
}