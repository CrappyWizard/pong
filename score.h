#pragma once
#include "SDL.h"
#include "SDL_ttf.h"
#include <stdio.h>

class Game;

class Score {
	public:
		Score();
		~Score();

		void AIScored();
		void PlayerScored();
		void DrawScore(SDL_Renderer* renderer_ptr);
	private :
		int player_score;
		int ai_score;
		TTF_Font* font;
};