#include "paddle.h"

Paddle::Paddle(float x, float y) :
	Object2D(),
	paddle_width(8),
	paddle_height(64),
	paddle_dir(0),
	paddle_speed(150.0f)
{
	Object2D::SetPosition(Vector2{ x, y });
	rect = { static_cast<int>(x), static_cast<int>(y), paddle_width, paddle_height };
	SDL_Log("x: %d\ny: %d\nw: %d\nh: %d\n\n", rect.x, rect.y, rect.w, rect.h);
}

Paddle::~Paddle() {
	// Nothing needs to be deleted yet
}

void Paddle::ProcessKeyboard(const uint8_t* state) {
	paddle_dir = 0;
	if (state[SDL_SCANCODE_W]) {
		paddle_dir = -1;
	}
	if (state[SDL_SCANCODE_S]) {
		paddle_dir = 1;
	}
}

void Paddle::AIDirection(Vector2 ball_pos) {
	paddle_dir = 0;
	Vector2 paddle_pos = Object2D::GetPosition();

	if ((paddle_pos.y + 24) > ball_pos.y) {
		paddle_dir += -1;
	}
	if ((paddle_pos.y + 40) < ball_pos.y) {
		paddle_dir += 1;
	}
}

void Paddle::Update(float delta) {
	Vector2 paddle_pos = Object2D::GetPosition();
	
	// Update paddle position
	paddle_pos.y += paddle_dir * delta * paddle_speed;
	
	rect.y = static_cast<int>(paddle_pos.y);
	Object2D::SetPosition(paddle_pos);
}

void Paddle::DrawObject2d (SDL_Renderer* renderer_ptr) {
	SDL_RenderFillRect(renderer_ptr, &rect);
}

int Paddle::GetHeight() {
	return paddle_height;
}

int Paddle::GetWidth() {
	return paddle_width;
}