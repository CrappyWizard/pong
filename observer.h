#pragma once
#include "object2d.h"
#include "event.h"

class Observer {
	public:
		virtual ~Observer() {};
		virtual void onNotify(const Object2D& object2d, Event event) = 0;
};