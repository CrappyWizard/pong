#pragma once
#include "SDL.h"
#include "SDL_ttf.h" // Might not be needed
#include "SDL_mixer.h"
#include "math.h"
#include "paddle.h"
#include "ball.h"
#include "score.h"
#include "object2d.h"
#include <vector>
#include <cmath>

class Paddle;
class Ball;
class Score;

class Game {
public:
	Game();
	// Initialize Game
	~Game();
	bool Initialize();
	// Run the game loop
	//void RunLoop();
	// Game Loop functions
	void ProcessInput();
	void UpdateGame();
	void GenerateOutput();

	int GetPaddleHeight();
	int GetThickness();

	bool KeepRunning();
	void AIScored();
	void PlayerScored();
private:
	void LoadData();
	void UnloadData();

	void BackgroundColor(SDL_Renderer* renderer_ptr);
	void ForegroundColor(SDL_Renderer* renderer_ptr);

	SDL_Window* window_ptr;
	SDL_Renderer* renderer_ptr;

	// Music variables
	Mix_Music* music_ptr;

	Uint32 lag_time;
	Uint32 previous_time;
	Uint32 tick_counter;

	int window_height;
	int	window_width;

	// Should the game keep running?
	bool is_running;

	// Pong specific ball
	Paddle* paddle_ptr;
	Paddle* paddleAI_ptr;
	Ball* ball_ptr;
	Score* score_ptr;
	int thickness;
	int paddle_height;
};