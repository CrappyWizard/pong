#include "score.h"
#include <string>

Score::Score() :
	player_score(0),
	ai_score(0),
	font(TTF_OpenFont("fonts/VT323-Regular.ttf", 64))
{}

Score::~Score() {
	TTF_CloseFont(font);
	// To do
}

void Score::PlayerScored() {
	player_score++;
}

void Score::AIScored() {
	ai_score++;
}

void Score::DrawScore(SDL_Renderer* renderer_ptr) {
	int text_width, text_height;
	std::string score_str = std::to_string(player_score) + '-' + std::to_string(ai_score);

	SDL_Color white = { 255, 255, 255, 0 };
	SDL_Surface* surface_message = TTF_RenderText_Solid(font, score_str.c_str(), white);
	SDL_Texture* score = SDL_CreateTextureFromSurface(renderer_ptr, surface_message);
	text_width = surface_message->w;
	text_height = surface_message->h;

	SDL_FreeSurface(surface_message);
	SDL_Rect* message_rect = new SDL_Rect();
	message_rect->x = 280;
	message_rect->y = 16;
	message_rect->w = text_width;
	message_rect->h = text_height;

	SDL_RenderCopy(renderer_ptr, score, NULL, message_rect);
	SDL_DestroyTexture(score);
	delete message_rect;
}