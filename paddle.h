#pragma once
#include "object2d.h"
#include "ball.h"
#include "math.h"
#include "SDL.h"

class Ball;

class Paddle : public Object2D {
public:
	SDL_Rect rect;

	Paddle(float x, float y);
	~Paddle();

	void ProcessKeyboard(const uint8_t* state);
	virtual void Update(float delta) override;
	void DrawObject2d(SDL_Renderer* renderer_ptr);
	void AIDirection(Vector2 ball_pos);

	int GetWidth();
	int GetHeight();
private:
	int paddle_width, paddle_height;
	int paddle_dir;
	bool is_player;
	float paddle_speed;
};