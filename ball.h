#pragma once
#include "paddle.h"
#include "object2d.h"
#include "math.h"
#include "SDL.h"

class Paddle;

class Ball : public Object2D{
public:
	SDL_Rect ball;

	Ball(float x, float y);
	virtual ~Ball();
	virtual void Update(float delta) override;
	void DrawObject2d(SDL_Renderer* renderer_ptr);
	void Collision(Paddle* paddle1, Paddle* paddle2);
private:
	Vector2 ball_vel;
	Vector2 ball_pos;
	int ball_thickness;
};