#include "ball.h"

Ball::Ball(float x, float y) :
	Object2D(),
	ball_thickness(8),
	ball_vel{200.0f, 100.0f}
{
	Object2D::SetPosition(Vector2{ x, y });
	ball = { static_cast<int>(x), static_cast<int>(y),ball_thickness, ball_thickness };
}

Ball::~Ball() {
	// Nothing needs to be deleted yet
}

void Ball::Update(float delta) {
	Vector2 ball_pos = Object2D::GetPosition();

	if (ball.y < 0 && ball_vel.y < 0) {
		ball_vel.y = ball_vel.y * -1;
	}
	// Window Height
	if (ball.y > 480 && ball_vel.y > 0) {
		ball_vel.y = ball_vel.y * -1;
	}

	// Update ball position
	ball_pos.x += ball_vel.x * delta;
	ball_pos.y += ball_vel.y * delta;

	ball.x = static_cast<int>(ball_pos.x);
	ball.y = static_cast<int>(ball_pos.y);

	Object2D::SetPosition(ball_pos);
}

void Ball::DrawObject2d(SDL_Renderer* renderer_ptr) {
	SDL_RenderFillRect(renderer_ptr, &ball);
}

void Ball::Collision(Paddle* paddle1, Paddle* paddle2) {
	Vector2 paddle1_pos = paddle1->GetPosition();
	Vector2 paddle2_pos = paddle2->GetPosition();
	
	// Calculate the absolute Y difference between the center of the paddle and the ball
	float diff = std::abs((paddle1_pos.y + paddle1->GetHeight() / 2) - ball.y);

	// Check for collisions
	if (diff <= paddle1->GetHeight() / 2 && ball.x <= (paddle1_pos.x + paddle1->GetWidth()) && ball.x >= (paddle1_pos.x + paddle1->GetWidth() - 4) && ball_vel.x < 0.0f) {
		ball_vel.x *= -1;
		// Every time the ball touches a paddle increase its speed
		ball_vel.x *= 1.05f;
		ball_vel.y *= 1.05f;
	}

	diff = std::abs((paddle2_pos.y + paddle2->GetHeight() / 2) - ball.y);

	// Check for collisions
	if (diff <= paddle2->GetHeight() / 2 && ball.x >= paddle2_pos.x  && ball.x <= (paddle2_pos.x + paddle2->GetWidth() - 4) && ball_vel.x > 0.0f) {
		ball_vel.x *= -1;
		// Every time the ball touches a paddle increase its speed
		ball_vel.x *= 1.05f;
		ball_vel.y *= 1.05f;
	}
}